<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Morton Jonuschat <m.jonuschat@mojocode.de>, MoJo Code
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Tx_MojoMasonry_Domain_Model_Wall.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage Masonry
 *
 * @author Morton Jonuschat <m.jonuschat@mojocode.de>
 */
class Tx_MojoMasonry_Domain_Model_WallTest extends Tx_Extbase_Tests_Unit_BaseTestCase {
	/**
	 * @var Tx_MojoMasonry_Domain_Model_Wall
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new Tx_MojoMasonry_Domain_Model_Wall();
	}

	public function tearDown() {
		unset($this->fixture);
	}

	/**
	 * @test
	 */
	public function getTitleReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setTitleForStringSetsTitle() { 
		$this->fixture->setTitle('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getTitle()
		);
	}
	
	/**
	 * @test
	 */
	public function getBricksReturnsInitialValueForObjectStorageContainingTx_MojoMasonry_Domain_Model_Brick() { 
		$newObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getBricks()
		);
	}

	/**
	 * @test
	 */
	public function setBricksForObjectStorageContainingTx_MojoMasonry_Domain_Model_BrickSetsBricks() { 
		$brick = new Tx_MojoMasonry_Domain_Model_Brick();
		$objectStorageHoldingExactlyOneBricks = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneBricks->attach($brick);
		$this->fixture->setBricks($objectStorageHoldingExactlyOneBricks);

		$this->assertSame(
			$objectStorageHoldingExactlyOneBricks,
			$this->fixture->getBricks()
		);
	}
	
	/**
	 * @test
	 */
	public function addBrickToObjectStorageHoldingBricks() {
		$brick = new Tx_MojoMasonry_Domain_Model_Brick();
		$objectStorageHoldingExactlyOneBrick = new Tx_Extbase_Persistence_ObjectStorage();
		$objectStorageHoldingExactlyOneBrick->attach($brick);
		$this->fixture->addBrick($brick);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneBrick,
			$this->fixture->getBricks()
		);
	}

	/**
	 * @test
	 */
	public function removeBrickFromObjectStorageHoldingBricks() {
		$brick = new Tx_MojoMasonry_Domain_Model_Brick();
		$localObjectStorage = new Tx_Extbase_Persistence_ObjectStorage();
		$localObjectStorage->attach($brick);
		$localObjectStorage->detach($brick);
		$this->fixture->addBrick($brick);
		$this->fixture->removeBrick($brick);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getBricks()
		);
	}
	
}
?>