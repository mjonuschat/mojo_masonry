<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Morton Jonuschat <m.jonuschat@mojocode.de>, MoJo Code
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package mojo_masonry
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_MojoMasonry_Controller_BrickController extends Tx_Extbase_MVC_Controller_ActionController {

  /**
   * brickRepository
   *
   * @var Tx_MojoMasonry_Domain_Repository_BrickRepository
   */
  protected $brickRepository;

  /**
   * injectBrickRepository
   *
   * @param Tx_MojoMasonry_Domain_Repository_BrickRepository $brickRepository
   * @return void
   */
  public function injectBrickRepository(Tx_MojoMasonry_Domain_Repository_BrickRepository $brickRepository) {
    $this->brickRepository = $brickRepository;
  }

  /**
   * action show
   *
   * @param Tx_MojoMasonry_Domain_Model_Brick $brick
   * @return void
   */
  public function showAction(Tx_MojoMasonry_Domain_Model_Brick $brick) {
    $this->view->assign('brick', $brick);
  }

}
