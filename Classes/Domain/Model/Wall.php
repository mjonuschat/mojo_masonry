<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Morton Jonuschat <m.jonuschat@mojocode.de>, MoJo Code
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package mojo_masonry
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_MojoMasonry_Domain_Model_Wall extends Tx_Extbase_DomainObject_AbstractEntity {

  /**
   * Title
   *
   * @var string
   * @validate NotEmpty
   */
  protected $title;

  /**
   * Bricks
   *
   * @var Tx_Extbase_Persistence_ObjectStorage<Tx_MojoMasonry_Domain_Model_Brick>
   */
  protected $bricks;

  /**
   * __construct
   *
   * @return void
   */
  public function __construct() {
    //Do not remove the next line: It would break the functionality
    $this->initStorageObjects();
  }

  /**
   * Initializes all Tx_Extbase_Persistence_ObjectStorage properties.
   *
   * @return void
   */
  protected function initStorageObjects() {
    /**
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     */
    $this->bricks = new Tx_Extbase_Persistence_ObjectStorage();
  }

  /**
   * Returns the title
   *
   * @return string $title
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Sets the title
   *
   * @param string $title
   * @return void
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * Adds a Brick
   *
   * @param Tx_MojoMasonry_Domain_Model_Brick $brick
   * @return void
   */
  public function addBrick(Tx_MojoMasonry_Domain_Model_Brick $brick) {
    $this->bricks->attach($brick);
  }

  /**
   * Removes a Brick
   *
   * @param Tx_MojoMasonry_Domain_Model_Brick $brickToRemove The Brick to be removed
   * @return void
   */
  public function removeBrick(Tx_MojoMasonry_Domain_Model_Brick $brickToRemove) {
    $this->bricks->detach($brickToRemove);
  }

  /**
   * Returns the bricks
   *
   * @return Tx_Extbase_Persistence_ObjectStorage<Tx_MojoMasonry_Domain_Model_Brick> $bricks
   */
  public function getBricks() {
    return $this->bricks;
  }

  /**
   * Sets the bricks
   *
   * @param Tx_Extbase_Persistence_ObjectStorage<Tx_MojoMasonry_Domain_Model_Brick> $bricks
   * @return void
   */
  public function setBricks(Tx_Extbase_Persistence_ObjectStorage $bricks) {
    $this->bricks = $bricks;
  }

}
