<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Morton Jonuschat <m.jonuschat@mojocode.de>, MoJo Code
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package mojo_masonry
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Tx_MojoMasonry_Domain_Model_Brick extends Tx_Extbase_DomainObject_AbstractEntity {
  const assetPath = 'uploads/tx_mojomasonry/';
  /*
   * Configuration Manager
   *
   * @var Tx_Extbase_Configuration_ConfigurationManagerInterface
   */
  protected $configurationManager;

  /*
   * Inject the Configuration Manager
   *
   * @param Tx_Extbase_Configuration_ConfigurationManagerInterface $configurationManager
   * @return void
   */
  public function injectConfigurationManager(Tx_Extbase_Configuration_ConfigurationManagerInterface $configurationManager) {
    $this->configurationManager = $configurationManager;
  }

  /**
   * Title
   *
   * @var string
   * @validate NotEmpty
   */
  protected $title;

  /**
   * Description
   *
   * @var string
   */
  protected $description;

  /**
   * Image
   *
   * @var string
   * @validate NotEmpty
   */
  protected $image;

  /**
   * Width
   *
   * @var integer
   * @validate NotEmpty
   */
  protected $width;

  /**
   * Returns the title
   *
   * @return string $title
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Sets the title
   *
   * @param string $title
   * @return void
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * Returns the description
   *
   * @return string $description
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Sets the description
   *
   * @param string $description
   * @return void
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * Returns the image
   *
   * @return string $image
   */
  public function getImage() {
    return self::assetPath . $this->image;
  }

  /**
   * Sets the image
   *
   * @param string $image
   * @return void
   */
  public function setImage($image) {
    $this->image = $image;
  }

  /**
   * Returns the width
   *
   * @return integer $width
   */
  public function getWidth() {
    $settings = $this->configurationManager->getConfiguration('Settings','MojoMasonry','Main');
    return $settings['brick']['width' . $this->width];
  }

  /**
   * Sets the width
   *
   * @param integer $width
   * @return void
   */
  public function setWidth($width) {
    $this->width = $width;
  }

}
