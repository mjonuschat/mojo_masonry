<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

Tx_Extbase_Utility_Extension::registerPlugin(
	$_EXTKEY,
	'Main',
	'Masonry'
);

t3lib_extMgm::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Masonry');

t3lib_extMgm::addLLrefForTCAdescr('tx_mojomasonry_domain_model_wall', 'EXT:mojo_masonry/Resources/Private/Language/locallang_csh_tx_mojomasonry_domain_model_wall.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_mojomasonry_domain_model_wall');
$TCA['tx_mojomasonry_domain_model_wall'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:mojo_masonry/Resources/Private/Language/locallang_db.xml:tx_mojomasonry_domain_model_wall',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'title,bricks,',
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Wall.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_mojomasonry_domain_model_wall.gif'
	),
);

t3lib_extMgm::addLLrefForTCAdescr('tx_mojomasonry_domain_model_brick', 'EXT:mojo_masonry/Resources/Private/Language/locallang_csh_tx_mojomasonry_domain_model_brick.xml');
t3lib_extMgm::allowTableOnStandardPages('tx_mojomasonry_domain_model_brick');
$TCA['tx_mojomasonry_domain_model_brick'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:mojo_masonry/Resources/Private/Language/locallang_db.xml:tx_mojomasonry_domain_model_brick',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'title,description,image,width,',
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY) . 'Configuration/TCA/Brick.php',
		'iconfile' => t3lib_extMgm::extRelPath($_EXTKEY) . 'Resources/Public/Icons/tx_mojomasonry_domain_model_brick.gif'
	),
);

?>